# XSA модель - шпрот черноморский в водах России 
> English version available below 

Биостатическая модель расширенного анализа пополнения (extended survivor analysis) в среде R с набором входных данных для выполнения полноценного анализа в среде R. В результате анализа вы получите сгенерированный отчет со всеми стадиями: Входные данные, первичная диагностика, построение модели, диагностика модели, уравнение запас-пополнение, биолоигческие ориентиры, сценарии прогноза. 


## 1. Требования
  1. R version >= 3.6.1
  2. FLR packages >= 2.6.2
  3. Devtools, Rtools
  4. Rstudio 1.1.xx (1.2 bugged)

Установка нужных пакетов:
```
# default R packages
install.packages(c("dplyr", "magrittr", "tibble", "fishmethods", "icesAdvice", "rmarkdown"))
# install FLR (for XSA)
install.packages(c("FLCore", "FLXSA", "FLAssess", "ggplotFL", "FLBRP", "FLash"), repos="http://flr-project.org/R")
# install EQSIM for SRR model simulations
library("devtools")
install_github("ices-tools-prod/msy")
```

## 2. Выполнение анализа
Входные данные для анализа расположены в папке `/input` в формате `.csv`.

Параметризация модели выполняется в начале файла `run.R`, секция config:
```
config.stock.name <- "Sprattus sprattus" # fish title
config.stock.desc <- "Sprat stock information in FLCore format from 2000years" # fish stock description
....
```

Для выполнения анализа запустите Rstudio и откройте проект из папки этого репозитория (файл с названием xsa.proj). Откройте файл `run.R` и выполните его (меню "Source"). 

Если у вас нет Rstudio вы можете выполнить анализ из консоли:
```
setwd("path/to/sprat-xsa/")
source("run.R")
```

Результаты анализа в текстовом и графическом варианте будут сохранены в папке `/output`. 

## 3. Построение отчета и результаты
Для построения результрирующего отчета (английский язык) выполните в консоли:
```
render("Result.Rmd")
```
В результате появится файл `Result.html`, который можно изучить при помощи браузера.

## 4. Информация и АП
  - [FLR-project](https://flr-project.org/)
  - [ICES MSY](https://github.com/ices-tools-prod/msy)
  - [ICES Advice](https://github.com/ices-tools-prod/icesAdvice)

## 5. Автор
Пятинский Михаил, Азово-Черноморский филиал ФГБНУ ВНИРО

Email: pyatinskiy_m_m@azniirkh.ru. Telegram: [@zenn1989](https://t.me/zenn1989). Twitter: [@followzenn](https://twitter.com/followzenn)


# English version: Black sea sprat stock assessment (Russian water fishing unit)
There is a complete XSA model approach with final report (markdown) generator are presented. Analysis are writen on R. The final report will summarise: preliminary diagnostics, model fitting, model diagnostics, SRR fitting, BRP calculation, forecasting scenarious and other. 


## 1. Requirements
  1. R version >= 3.6.1
  2. FLR packages >= 2.6.2
  3. Devtools, Rtools
  4. Rstudio 1.1.xx (1.2 bugged)

Install required packages:
```
# default R packages
install.packages(c("dplyr", "magrittr", "tibble", "fishmethods", "icesAdvice", "rmarkdown"))
# install FLR (for XSA)
install.packages(c("FLCore", "FLXSA", "FLAssess", "ggplotFL", "FLBRP", "FLash"), repos="http://flr-project.org/R")
# install EQSIM for SRR model simulations
library("devtools")
install_github("ices-tools-prod/msy")
```

## 2. Performing analysis
Input data (stock and indices) are placed at `/input` directory in `.csv` format.

After preparing input data - parametrize model by config section in `run.R` head:
```
config.stock.name <- "Sprattus sprattus" # fish title
config.stock.desc <- "Sprat stock information in FLCore format from 2000years" # fish stock description
...
```

To run assassment the best choice is run Rstudio and open project from this repository (file with name sprat-bsea.proj). Then in file manager (Rstudio) open `run.R` and execute it by press "Source" button. 

If you dont want to use Rstudio you can run it directly by:
```
setwd("path/to/sprat-xsa/")
source("run.R")
```

All results will be saved in `/output` directory. 

## 3. Build report
To build the final report with markdown you should run:
```
render("Result.Rmd")
```

and you will found `Result.html` in root project directory. You can read the report by any kind of web-browser.

## 4. Credintials
  - [FLR-project](https://flr-project.org/)
  - [ICES MSY](https://github.com/ices-tools-prod/msy)
  - [ICES Advice](https://github.com/ices-tools-prod/icesAdvice)

## 5. Author
Mikhail Piatinskii, Azov-Black sea branch of VNIRO 

Email: pyatinskiy_m_m@azniirkh.ru. Telegram: [@zenn1989](https://t.me/zenn1989). Twitter: [@followzenn](https://twitter.com/followzenn)