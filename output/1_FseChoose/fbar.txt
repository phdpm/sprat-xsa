[1] "============= Fbar at Fse = 0.5"
An object of class "FLQuant"
, , unit = unique, season = all, area = unique

     year
age   1994    1995    1996    1997    1998    1999    2000    2001    2002    2003   
  all 1.54340 0.76457 1.33777 0.67779 1.08739 0.15518 0.31365 0.30854 0.30711 0.49200
     year
age   2004    2005    2006    2007    2008    2009    2010    2011    2012    2013   
  all 0.52660 0.92905 0.79005 0.59545 1.41268 2.48999 1.53707 1.15503 1.22426 0.38950
     year
age   2014    2015    2016    2017    2018    2019    2020   
  all 0.21280 0.77636 0.96163 0.31811 0.73255 0.53999 0.68911

units:  f 

[1] "============= Fbar at Fse = 1"
An object of class "FLQuant"
, , unit = unique, season = all, area = unique

     year
age   1994    1995    1996    1997    1998    1999    2000    2001    2002    2003   
  all 1.54340 0.76457 1.33777 0.67779 1.08739 0.15518 0.31365 0.30854 0.30711 0.49200
     year
age   2004    2005    2006    2007    2008    2009    2010    2011    2012    2013   
  all 0.52660 0.92904 0.79004 0.59543 1.41255 2.48748 1.52747 1.11712 1.13403 0.34201
     year
age   2014    2015    2016    2017    2018    2019    2020   
  all 0.20036 0.71916 0.76622 0.34147 0.77601 0.56646 0.67236

units:  f 

[1] "============= Fbar at Fse = 1.5"
An object of class "FLQuant"
, , unit = unique, season = all, area = unique

     year
age   1994    1995    1996    1997    1998    1999    2000    2001    2002    2003   
  all 1.54340 0.76457 1.33777 0.67779 1.08739 0.15518 0.31365 0.30854 0.30711 0.49200
     year
age   2004    2005    2006    2007    2008    2009    2010    2011    2012    2013   
  all 0.52660 0.92903 0.79002 0.59539 1.41233 2.48301 1.51061 1.05255 1.01083 0.28798
     year
age   2014    2015    2016    2017    2018    2019    2020   
  all 0.18479 0.67501 0.68726 0.38389 0.86237 0.53152 0.68290

units:  f 

[1] "============= Fbar at Fse = 2"
An object of class "FLQuant"
, , unit = unique, season = all, area = unique

     year
age   1994    1995    1996    1997    1998    1999    2000    2001    2002    2003   
  all 1.54340 0.76457 1.33777 0.67779 1.08739 0.15518 0.31365 0.30854 0.30711 0.49200
     year
age   2004    2005    2006    2007    2008    2009    2010    2011    2012    2013   
  all 0.52660 0.92904 0.79003 0.59540 1.41237 2.48377 1.51348 1.06167 1.03210 0.30021
     year
age   2014    2015    2016    2017    2018    2019    2020   
  all 0.18967 0.69627 0.70414 0.39291 0.88872 0.54790 0.71278

units:  f 

[1] "============= Fbar at Fse = 2.5"
An object of class "FLQuant"
, , unit = unique, season = all, area = unique

     year
age   1994    1995    1996    1997    1998    1999    2000    2001    2002    2003   
  all 1.54340 0.76457 1.33777 0.67779 1.08739 0.15518 0.31365 0.30854 0.30711 0.49200
     year
age   2004    2005    2006    2007    2008    2009    2010    2011    2012    2013   
  all 0.52660 0.92904 0.79003 0.59540 1.41241 2.48462 1.51667 1.07239 1.05601 0.31357
     year
age   2014    2015    2016    2017    2018    2019    2020   
  all 0.19479 0.71727 0.72219 0.39974 0.91095 0.56248 0.73948

units:  f 

