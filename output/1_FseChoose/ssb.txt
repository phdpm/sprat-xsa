[1] "============= SSB at Fse = 0.5"
An object of class "FLQuant"
, , unit = unique, season = all, area = unique

     year
age   1994   1995   1996   1997   1998   1999   2000   2001   2002   2003   2004  
  all  45936  46885  48648  90764 158063 202166 408827 348800 326395 218474 168863
     year
age   2005   2006   2007   2008   2009   2010   2011   2012   2013   2014   2015  
  all 132937  87051  77612  83500  62614  49624  43577  41181  70017  66671 100420
     year
age   2016   2017   2018   2019   2020  
  all  78277  80287  69014  68480  55742

units:  NA * NA NA * numbers NA * NA NA * t NA * probability 

[1] "============= SSB at Fse = 1"
An object of class "FLQuant"
, , unit = unique, season = all, area = unique

     year
age   1994   1995   1996   1997   1998   1999   2000   2001   2002   2003   2004  
  all  45936  46885  48648  90764 158063 202166 408828 348800 326395 218474 168864
     year
age   2005   2006   2007   2008   2009   2010   2011   2012   2013   2014   2015  
  all 132937  87051  77614  83504  62697  49724  45204  42743  71768  71481 101348
     year
age   2016   2017   2018   2019   2020  
  all  78683  78189  67437  67325  59770

units:  NA * NA NA * numbers NA * NA NA * t NA * probability 

[1] "============= SSB at Fse = 1.5"
An object of class "FLQuant"
, , unit = unique, season = all, area = unique

     year
age   1994   1995   1996   1997   1998   1999   2000   2001   2002   2003   2004  
  all  45936  46885  48648  90764 158063 202166 408828 348800 326396 218474 168864
     year
age   2005   2006   2007   2008   2009   2010   2011   2012   2013   2014   2015  
  all 132939  87052  77617  83512  62837  49885  48925  46344  74111  78919 102855
     year
age   2016   2017   2018   2019   2020  
  all  79039  81502  67069  67916  64375

units:  NA * NA NA * numbers NA * NA NA * t NA * probability 

[1] "============= SSB at Fse = 2"
An object of class "FLQuant"
, , unit = unique, season = all, area = unique

     year
age   1994   1995   1996   1997   1998   1999   2000   2001   2002   2003   2004  
  all  45936  46885  48648  90764 158063 202166 408828 348800 326396 218474 168864
     year
age   2005   2006   2007   2008   2009   2010   2011   2012   2013   2014   2015  
  all 132938  87052  77617  83510  62810  49848  48428  45266  72852  77474 101254
     year
age   2016   2017   2018   2019   2020  
  all  78103  80102  65970  66934  64638

units:  NA * NA NA * numbers NA * NA NA * t NA * probability 

[1] "============= SSB at Fse = 2.5"
An object of class "FLQuant"
, , unit = unique, season = all, area = unique

     year
age   1994   1995   1996   1997   1998   1999   2000   2001   2002   2003   2004  
  all  45936  46885  48648  90764 158063 202166 408828 348800 326395 218474 168864
     year
age   2005   2006   2007   2008   2009   2010   2011   2012   2013   2014   2015  
  all 132938  87052  77616  83509  62781  49810  47831  44191  71691  75934  99768
     year
age   2016   2017   2018   2019   2020  
  all  77248  78860  65099  66097  64356

units:  NA * NA NA * numbers NA * NA NA * t NA * probability 

