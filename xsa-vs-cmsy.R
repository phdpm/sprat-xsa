library(ggplot2)

# build fishing mortality uncertainty data
pics <- FLQuants(
  Rec = rnorm(1000, rec(result.xsa.stock), result.xsa.sigma * rec(result.xsa.stock)),
  SSB = rnorm(1000, ssb(result.xsa.stock), result.xsa.sigma * ssb(result.xsa.stock)),
  F = rnorm(1000, fbar(result.xsa.stock), result.xsa.sigma * fbar(result.xsa.stock)),
  Catch = catch(result.xsa.stock)
)

# get ssb
xsa.ssb <- pics$SSB
# read cmsy output from table
cmsy.b <- read_clip_tbl()

plot(xsa.ssb) + 
  geom_line(data = cmsy.b, aes(y = b), linetype = "dashed") + 
  geom_ribbon(data = cmsy.b, aes(y = b, ymin = b_lo, ymax = b_hi), alpha = 0.15, fill = "blue") + 
  theme_minimal()